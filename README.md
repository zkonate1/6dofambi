# 6dofAmbi

Workplace for experimentation with 6dof navigable ambisonics soundfields, such as those captured by the Zylia.


the audiofiles are not included in this repo, but can be downloaded from:

http://www.zylia.co/zylia-6dof-hoa.html

and installed to a subdirectory called

zylia-6dof-hoa (which should contain Zylia-Sledz-Island and Zylia-Sledz-Tense)


